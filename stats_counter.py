import sys, os, re
from math import ceil, floor
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

required_info ="(?P<name>\w+\s\w+)\sbatted\s(?P<bat>\d)\stimes\swith\s(?P<hit>\d)\shits\sand\s\d\sruns"

def float_round(num, places = 0, direction = floor):
    return direction(num * (10**places)) / float(10**places)

hit={}
bat={}
avg={}

f=open(filename)
for line in f:
	result = re.match(required_info, line) #get required information:name,bat,hit
	if result:#if get succeed
		if result.group('name') in bat:
			bat[result.group('name')] += float(result.group('bat'))
			hit[result.group('name')] += float(result.group('hit'))
		else:
			bat[result.group('name')] = float(result.group('bat'))
			hit[result.group('name')] = float(result.group('hit'))

f.close()
for name in hit:
	avg_num=hit[name]/bat[name]
	avg[name] = float(round(avg_num, 3))
	
for name in sorted(avg, key=avg.get, reverse=True):
	print "%s: %.3f" % (name, avg[name])